from time import sleep
import pytest
import allure
from selenium.webdriver.common.by import By

from shizhan.page.base_page import Base
from shizhan.page.index_page import Index

@allure.feature('企业微信相关测试')


class Test_Wechat():
    # def setup_class(self):
    #     Main().goto_login().goto_index().goto_contact().goto_add_department()

    def teardown(self):
        Base().find(By.ID, 'menu_index').click()

    @pytest.mark.parametrize(("username", "acctid", "phone"), [('周', '3333', '15425485455')])
    @allure.story('检测是否添加成员成功')
    def test_add_member(self, username, acctid, phone):
        names = Index().goto_contact().goto_add_member().make_add_member(username, acctid, phone).judge_add_member_success()
        assert username in names

    @allure.story('检测是否登录成功')
    def test_login(self):
        name = Index().judge_login_success()
        assert name == '我的企业'

    @pytest.mark.parametrize('department_name', [
                                                ('单元测试'),
                                                ('冒烟测试'),
                                                ('集成测试')]
                             )
    @allure.story('检测添加部门是否成功')
    def test_add_department(self, department_name):
        toast = Index().goto_contact().goto_add_department().make_add_department(department_name).judge_add_department_success()
        assert '部门添加成功' in toast

    def test_import_contact(self):
        done = Index().goto_contact().goto_import_contact().make_import_contact('E:\\python_shizhan\\test_selenium\\selenium\\data\\contact.xls').judge_import_contact_success()
        assert '完成' in done