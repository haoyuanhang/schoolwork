from time import sleep
from selenium.webdriver.common.by import By
from page.base_page import Base


class Add_Department(Base):

    def make_add_department(self, department_name):
        # 输入部门名称
        self.find(By.XPATH, '//*[@id="__dialog__MNDialog__"]/div/div[2]/div/form/div[1]/input').send_keys(department_name)
        # 选择所属部门
        self.find(By.XPATH, '//*[@class="js_parent_party_name"]').click()
        sleep(1)

        # 选择测试部门
        
        self.driver.find_elements_by_xpath('//*[@class="jstree jstree-43 jstree-default"]//a')[1].click()
        # self.find(By.XPATH, '//*[@class="jstree jstree-40 jstree-default"]//a[@id="1688850580024685_anchor"]').click()
        # self.find(By.XPATH, '//*[@class="qui_dialog_body ww_dialog_body"]//a[@id="1688850580024685_anchor"]').click()
        # 点击确定
        self.find(By.XPATH, '//*[@class="qui_dialog_foot ww_dialog_foot"]/a[@d_ck="submit"]').click()
        sleep(1)
        self.driver.save_screenshot(f"../img/add_department/{department_name}.png")
        sleep(2)
        return self

    def judge_add_department_success(self):
        self.wdtec(3, '//*[@id="js_tips"]')
        # WebDriverWait(self.driver, 3).until(expected_conditions.visibility_of_element_located((By.XPATH, '//*[@id="js_tips"]')))
        toast = self.find(By.XPATH, '//*[@id="js_tips"]').text
        return toast


