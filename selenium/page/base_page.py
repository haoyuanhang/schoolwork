from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By


class Base:

    def __init__(self, driver: WebDriver=None):
        if driver is None:
            option = Options()
            option.debugger_address = '127.0.0.1:9222'
            self.driver = webdriver.Chrome(options=option)
        else:
            self.driver = driver
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def find(self, by, loc):
        return self.driver.find_element(by, loc)

    # 对于可见、可点击、加载到dom等分别建立方法
    def wdtec(self, wait_time, loc):
        return WebDriverWait(self.driver, wait_time).until(expected_conditions.visibility_of_element_located((By.XPATH, loc)))

    def wdwec_click(self, wait_time, loc):
        return WebDriverWait(self.driver, wait_time).until(expected_conditions.element_to_be_clickable((By.XPATH, loc)))
