from time import sleep
from selenium.webdriver.common.by import By
from page.base_page import Base


class Import_Contact(Base):

    def make_import_contact(self, file):
        # 为了是的上传文件的标签为input，先点击下面的填写模板
        loc1 = '//*[@class="js_go_template_import"]'
        self.wdtec(20, loc1)
        # WebDriverWait(self.driver, 20).until(expected_conditions.visibility_of_element_located((By.XPATH, loc1)))
        self.find(By.XPATH, loc1).click()
        sleep(3)

        # 等待上传按钮出现，并send_keys
        loc2 = '//*[@class="ww_fileImporter_fileContainer_uploadInputMask"]'
        self.wdtec(20, loc2)
        # WebDriverWait(self.driver, 20).until(expected_conditions.visibility_of_element_located((By.XPATH, loc2)))
        self.find(By.XPATH, loc2).send_keys(file)
        sleep(3)

        # 等待导入钮出现，并click()
        loc3 = '//*[@class="ww_fileImporter_submitWrap"]/a'
        self.wdtec(20, loc3)
        # WebDriverWait(self.driver, 20).until(expected_conditions.visibility_of_element_located((By.XPATH, loc3)))
        self.find(By.XPATH, loc3).click()
        sleep(5)
        self.driver.save_screenshot(f"../img/import_contact/{file[:-4]}.png")
        sleep(1)
        return self

    def judge_import_contact_success(self):
        return self.find(By.XPATH, '//*[@class="ww_fileImporter_successBtnWrap"]/a').text

