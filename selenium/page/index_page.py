from selenium.webdriver.common.by import By
from page.add_member_page import Add_Member
from page.base_page import Base
from page.contact_page import Contact


# 登录成功后的首页
class Index(Base):

    def goto_add_member(self):
        self.find(By.XPATH, '//*[@id="_hmt_click"]/div[1]/div[4]/div[2]/a[1]/div/span[2]').click()
        return Add_Member(self.driver)

    def goto_contact(self):
        self.find(By.XPATH, '//*[@id="menu_contacts"]/span').click()
        return Contact(self.driver)

    # 用来判断是否登录到用户首页
    def judge_login_success(self):
        # 返回页面种是否有“我的企业”用来断言
        git_name = self.find(By.XPATH, '//*[@id="menu_profile"]/span').text
        return git_name
