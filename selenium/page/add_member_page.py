from time import sleep
from selenium.webdriver.common.by import By
from page.base_page import Base

# 添加成员
class Add_Member(Base):
    # 添加成员操作并截图
    def make_add_member(self, username, acctid, phone):
        self.find(By.ID, 'username').send_keys(username)
        self.find(By.ID, 'memberAdd_acctid').send_keys(acctid)
        self.find(By.ID, 'memberAdd_phone').send_keys(phone)
        self.find(By.CSS_SELECTOR, '.qui_btn.ww_btn.js_btn_save').click()
        sleep(1)
        self.driver.save_screenshot(f"../img/add_member/{username}.png")
        return self

    # 验证添加成员操作是否成功
    def judge_add_member_success(self):
        name_list = self.driver.find_elements(By.XPATH, '//*[@class="member_colRight_memberTable_td"]//span')
        names = []
        for name in name_list:
            names.append(name.text)
        return names