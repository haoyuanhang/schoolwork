from time import sleep

from selenium.webdriver.common.by import By

from page.add_department_page import Add_Department
from page.add_member_page import Add_Member
from page.base_page import Base
from page.import_contact_page import Import_Contact

# 通讯录页面
class Contact(Base):

    def goto_add_member(self):
        loc = '//*[@class="ww_operationBar"]//a[@class="qui_btn ww_btn js_add_member"]'
        self.wdwec_click(10, loc)
        # WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_clickable((By.XPATH, loc)))
        self.find(By.XPATH, loc).click()
        return Add_Member(self.driver)

    def goto_add_department(self):
        # click = self.driver.execute_script('return document.querySelector("#js_contacts527 > div > div.member_colLeft > ul > li:nth-child(1) > a")')
        # click.click()
        loc = '//*[@class="member_colLeft_top_addBtn"]'
        self.wdtec(10, loc)
        # WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_clickable((By.XPATH, loc)))
        self.find(By.XPATH, loc).click()
        self.find(By.XPATH, '//*[@class="js_create_party"]').click()
        sleep(4)
        return Add_Department(self.driver)

    def goto_import_contact(self):
        loc = '//*[@class="ww_operationBar"]//div[@class="ww_btn_PartDropdown_left"]'
        self.wdtec(10, loc)
        # WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_clickable((By.XPATH, loc)))
        self.find(By.XPATH, loc).click()
        self.find(By.XPATH, '//*[@class="ww_operationBar"]//a[@class="qui_dropdownMenu_itemLink ww_dropdownMenu_itemLink js_import_member"]').click()
        sleep(3)
        return Import_Contact(self.driver)