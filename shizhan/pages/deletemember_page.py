from time import sleep

from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from shizhan.pages.base_page import BasePage, Logger


class DeletememberPage(BasePage):
    logger = Logger('delete.log')

    def make_deletemember(self, name):
        self.find(MobileBy.ID, 'h8g').click()
        self.logger.debug('点击更多')
        self.find(MobileBy.ID, 'b49').click()
        self.logger.debug('点击编辑成员')
        self.find(MobileBy.ID, 'e37').click()
        self.logger.debug('点击删除成员')
        self.find(MobileBy.ID, 'bei').click()
        self.logger.debug('点击确定')
        sleep(1)
        issay = (MobileBy.XPATH, '//*[@text="我的客户"]')
        WebDriverWait(self.driver, 5).until(expected_conditions.visibility_of_element_located((issay)))
        self.logger.debug('等待进入我的客户界面')
        self.driver.get_screenshot_as_file(f'../data/img/deletemember/{name}.png')
        self.logger.debug('在我的客户截图，看还没有没有该成员')

    def judge(self):
        elements = self.driver.find_elements(MobileBy.XPATH, '//*[@class="android.widget.TextView"]')
        names = []
        for element in elements:
            names.append(element.text)
        return names
