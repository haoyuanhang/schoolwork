from appium.webdriver.common.mobileby import MobileBy

from shizhan.pages.base_page import BasePage
from shizhan.pages.contact_page import ContactPage


class IndexPage(BasePage):
    def goto_contact(self):
        self.steps('../data/page_yaml/index_page.yaml')
        return ContactPage(self.driver)
