import logging

import yaml
from appium.webdriver.webdriver import WebDriver


class BasePage:
    def __init__(self, driver: WebDriver = None):
        self.driver = driver

    def find(self, by, loctor):
        return self.driver.find_element(by, loctor)

    def swipe_find(self, text):
        return self.driver.find_element_by_android_uiautomator(f'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text("{text}").instance(0));').click()

    def steps(self, path):
        with open(path, encoding='utf-8') as f:
            steps = yaml.safe_load(f)
        for step in steps:
            if 'by' in step.keys():
                element = self.find(step['by'], step['loctor'])
            if 'action' in step.keys():
                action = step['action']
                if action == 'click':
                    element.click()


    # def logger(self, path, clevel=logging.DEBUG, Flevel=logging.DEBUG):
    #     logger = logging.getLogger(path)
    #     logger.setLevel(logging.DEBUG)
    #     fmt = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s', '%Y-%m-%d %H:%M:%S')
    #     sh = logging.StreamHandler()
    #     sh.setFormatter(fmt)
    #     sh.setLevel(clevel)
    #     # 设置文件日志
    #     fh = logging.FileHandler(path)
    #     fh.setFormatter(fmt)
    #     fh.setLevel(Flevel)
    #     logger.addHandler(sh)
    #     logger.addHandler(fh)

class Logger:
    def __init__(self, path, clevel=logging.DEBUG, Flevel=logging.DEBUG):
        #
        self.logger = logging.getLogger(path)
        # 指定记录器将处理的最低严重性日志消息
        self.logger.setLevel(logging.DEBUG)
        # 格式化日志消息
        fmt = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s', '%Y-%m-%d %H:%M:%S')
        # 设置CMD日志
        sh = logging.StreamHandler()
        sh.setFormatter(fmt)
        sh.setLevel(clevel)
        # 设置文件日志
        fh = logging.FileHandler(path)
        fh.setFormatter(fmt)
        fh.setLevel(Flevel)
        self.logger.addHandler(sh)
        self.logger.addHandler(fh)

    def debug(self, message):
        self.logger.debug(message)

    def info(self, message):
        self.logger.info(message)

    def warn(self, message):
        self.logger.warn(message)

    def error(self, message):
        self.logger.error(message)

    def cri(self, message):
        self.logger.critical(message)
