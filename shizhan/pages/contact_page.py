from appium.webdriver.common.mobileby import MobileBy

from shizhan.pages.addmember_page import AddmemberPage
from shizhan.pages.base_page import BasePage
from shizhan.pages.deletemember_page import DeletememberPage


class ContactPage(BasePage):
    def goto_addmember(self):
        self.swipe_find('添加成员')
        return AddmemberPage(self.driver)

    def goto_deletemember(self, name):
        self.find(MobileBy.XPATH, f'//*[@text="{name}"]').click()
        return DeletememberPage(self.driver)