from appium.webdriver.common.mobileby import MobileBy

from shizhan.pages.base_page import BasePage
from shizhan.pages.handaddmember_page import HandAddmemberPage


class AddmemberPage(BasePage):
    def goto_hand_addmember(self):
        self.find(MobileBy.ID, 'hfj').click()
        return HandAddmemberPage(self.driver)