import logging
from time import sleep

from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from shizhan.pages.base_page import BasePage, Logger


class HandAddmemberPage(BasePage):
    logger = Logger('add.log')
    def make_addmember(self, name, phone_number):
        self.find(MobileBy.ID, 'ays').send_keys(name)
        self.logger.debug(f'输入姓名{name}')
        self.find(MobileBy.ID, 'f4m').send_keys(phone_number)
        self.logger.debug(f'输入电话号码{phone_number}')
        self.find(MobileBy.ID, 'ac9').click()
        self.logger.debug(f'点击确定')
        # 返回后用于验证
        sleep(1)
        self.find(MobileBy.ID, 'h86').click()
        self.logger.debug(f'返回')
        self.driver.get_screenshot_as_file(f'../data/img/addmember/{name}.png')
        self.logger.debug('这里进行了截图')
        return self

    def judge(self):
        loctor = '//*[@text="我的客户"]'
        WebDriverWait(self.driver, 5).until(expected_conditions.visibility_of_element_located((MobileBy.XPATH, loctor)))
        elements = self.driver.find_elements(MobileBy.XPATH, '//*[@class="android.widget.TextView"]')
        names = []
        for element in elements:
            names.append(element.text)
        return names
