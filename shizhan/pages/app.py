from appium import webdriver
from shizhan.pages.base_page import BasePage
from shizhan.pages.index_page import IndexPage


class App(BasePage):
    appPackage = 'com.tencent.wework'
    appActivity = '.launch.WwMainActivity'

    def start(self):
        if self.driver is None:
            caps = {
                "platformName": "android",
                "platformVersion": "6.0",
                "deviceName": "127.0.0.1:7555",
                "appPackage": self.appPackage,
                "appActivity": self.appActivity,
                "dontStopAppOnReset": True,
                "noReset": True,
            }
            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
            self.driver.implicitly_wait(10)
        else:
            self.driver.start_activity(self.appPackage, self.appActivity)
        print('App启动一次')
        return self

    def restart(self):
        pass

    def stop(self):
        self.driver.quit()

    def goto_index(self):
        return IndexPage(self.driver)
