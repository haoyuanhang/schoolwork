from time import sleep

import pytest
import yaml
from shizhan.pages.contact_page import ContactPage
from shizhan.pages.app import App


def load_test_yaml():
    with open('./addmember.yaml', encoding='utf-8') as f:
        return yaml.safe_load(f)



class TestContact:
    def setup_class(self):
        self.app = App().start().goto_index().goto_contact()

    @pytest.mark.parametrize('name, phone_number', load_test_yaml()['add'])
    def test_goto_handaddmember(self, name, phone_number):
        sleep(2)
        names = self.app.goto_addmember().goto_hand_addmember().make_addmember(name, phone_number).judge()
        assert name in names

    @pytest.mark.parametrize('name', load_test_yaml()['delete'])
    def test_goto_deleteaddmember(self, name):
        sleep(1)
        self.app.goto_deletemember(name).make_deletemember(name)