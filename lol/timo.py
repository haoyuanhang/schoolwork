from heroes import Heroes


class Timo(Heroes):
    # 重写英雄属性，使之成为‘提莫’英雄
    name = 'Timo'
    hp = 200
    power = 22
    speak = '提莫队长正在待命'


if __name__ == '__main__':
    Timo().speak_lines()
