from timo import Timo
from police import Police


class Hero_factory:

    # 实例英雄
    def create_hero(self, name):
        if name == 'Tiom':
            return Timo()
        elif name == 'Police':
            return Police()
        else:
            print('没有该英雄')


if __name__ == '__main__':
    timo = Hero_factory().create_hero('Tiom')
    police = Hero_factory().create_hero('Police')
    timo.fight(police.hp, police.power)
