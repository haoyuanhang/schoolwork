from heroes import Heroes


class Police(Heroes):
    # 重写英雄属性，使之成为‘女警’英雄
    name = 'Police'
    hp = 220
    power = 20
    speak = '见识一下法律的子弹'


if __name__ == '__main__':
    Police().speak_lines()
