"""

"""

class Heroes:
    # 定义类变量，在英雄继承的时候重写
    name = ''
    hp = ''
    power = ''
    speak = ''

    # 英雄打架方法
    def fight(self, enemy_hp, enemy_power):
        my_hp = self.hp - enemy_power
        enemy_final_hp = enemy_hp - self.power
        if my_hp > enemy_final_hp:
            print(f'{self.name}赢了')
        elif my_hp < enemy_final_hp:
            print('敌人赢了')
        else:
            print('平局')
    # 英雄台词方法
    def speak_lines(self):
        print(self.speak)


if __name__ == '__main__':
    timo = Heroes()
    timo.speak_lines()
