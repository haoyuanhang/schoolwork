from typing import List

import pytest


@pytest.fixture(autouse='Ture')
def step():
    print('开始计算')
    yield
    print('结束计算')


# 收集用例的hook函数
def pytest_collection_modifyitems(session, config, items: List) -> None:
    for item in items:
        # 用例名字改成中文
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        # 用例路径改成中文
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')
