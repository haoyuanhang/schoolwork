import allure
import pytest
from testing.calc import Calculator
import yaml


def calc_data():
    with open('./data.yaml', encoding='utf-8') as f:
        data = yaml.safe_load(f)
        return data


@allure.feature('计算测试')
class Test_calc:
    def setup_class(self):
        self.calc = Calculator()
        print('测试前实例')

    @pytest.mark.parametrize(("a", "b", "c"),
                             calc_data()['calc']['add']['values'],
                             ids=calc_data()['calc']['add']['ids'])
    @allure.story('加法测试')
    def test_add(self, a, b, c):
        assert self.calc.add(a, b) == c

    @pytest.mark.parametrize(("a", "b", "c"),
                             calc_data()['calc']['div']['values'],
                             ids=calc_data()['calc']['div']['ids'])
    @allure.story('除法测试')
    def test_div(self, a, b, c):
        assert self.calc.div(a, b) == c

